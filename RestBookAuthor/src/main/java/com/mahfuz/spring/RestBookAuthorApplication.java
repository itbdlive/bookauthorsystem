package com.mahfuz.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.mahfuz"})
public class RestBookAuthorApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestBookAuthorApplication.class, args);
	}

}
