package com.mahfuz.spring.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mahfuz.spring.model.Book;

public class BookRowMapper implements RowMapper<Book> {
	
	@Override
	public Book mapRow(ResultSet rs, int arg1) throws SQLException {
		Book book = new Book();
		book.setBookid(rs.getString("bookId"));
		book.setBookname(rs.getString("bookName"));
	   return book;
	}
}
