package com.mahfuz.spring.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.mahfuz.spring.model.Author;
import com.mahfuz.spring.model.Book;
import com.mahfuz.spring.model.BookAuthor;


public class BookAuthorRowMapper implements RowMapper<BookAuthor>{

	@Override
	public BookAuthor mapRow(ResultSet row, int rowNum) throws SQLException {
		BookAuthor bookauthor = new BookAuthor();
		Book book = bookauthor.getBook();
		Author author = bookauthor.getAuthor();
		book.setBookid(row.getString("bookId"));
		book.setBookname(row.getString("bookName"));
		author.setAuthorname(row.getString("authorName"));
		author.setAuthorid(row.getString("authorId"));
	   return bookauthor;
	}

}
