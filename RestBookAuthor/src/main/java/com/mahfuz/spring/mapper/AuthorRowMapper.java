package com.mahfuz.spring.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mahfuz.spring.model.Author;

public class AuthorRowMapper implements RowMapper<Author>{
	
	@Override
	public Author mapRow(ResultSet row, int arg1) throws SQLException {
		Author author = new Author();
		author.setAuthorid(row.getString("authorId"));
		author.setAuthorname(row.getString("authorName"));
	   return author;
	}
}
