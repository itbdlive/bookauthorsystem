package com.mahfuz.spring.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mahfuz.spring.dao.AuthorDao;
import com.mahfuz.spring.dao.BookAuthorDao;
import com.mahfuz.spring.dao.BookDao;
import com.mahfuz.spring.model.Author;
import com.mahfuz.spring.model.Book;
import com.mahfuz.spring.model.BookAuthor;

@Controller
public class BookRestController{
	
	@Autowired
	BookDao bookdao;
	
	@RequestMapping(method = RequestMethod.GET, value="/book/getall")
	  @ResponseBody
	  public List<Book> getAllBooks() {
		System.out.println("request send for book ");
		  return bookdao.findAllBook(); 
	  }
	
	@RequestMapping(method = RequestMethod.GET, value="/book/getid")
	  @ResponseBody
	  public List<Book> getBookid(@RequestParam(value="bookid", required=true) String bookid) {
		return bookdao.findbyBookid(bookid);
		  //return book;
	  }
	
	@RequestMapping(method = RequestMethod.POST, value="/book/create")
	  @ResponseBody
	  public Book addBook(@RequestBody Book book) {
		System.out.println("getting book id"+book.getBookid());
		System.out.println("getting book name"+book.getBookname());
		  bookdao.insertBook(book);
		  return book;
	  }
	
	@RequestMapping(method = RequestMethod.POST, value="/book/update")
	  @ResponseBody
	  public Book updateBookById(@RequestBody Book book) {
		  bookdao.updateBook(book);
		  return book;
	  }
	
	@RequestMapping(method = RequestMethod.DELETE, value="/book/delete")
	  @ResponseBody
	  public String deleteBookById(@RequestParam(value="bookid", required=true) String bookid) {
		  bookdao.deleteBook(bookid);
		  return bookid;
	  }
	
	
	@Autowired
	AuthorDao authordao;
	@RequestMapping(method = RequestMethod.GET, value="/author/getall")
	  @ResponseBody
	  public List<Author> getAllAuthors() {
		  return authordao.findAllAuthor(); 
	  }
	
	@RequestMapping(method = RequestMethod.GET, value="/author/getid")
	  @ResponseBody
	  public List<Author> getAuthorid(@RequestParam(value="authorid", required=true) String authorid) {
		return authordao.findbyAuthorid(authorid);
	  }
	
	@RequestMapping(method = RequestMethod.POST, value="/author/create")
	  @ResponseBody
	  public Author addAuthor(@RequestBody Author author) {
		authordao.insertAuthor(author);
		  return author;
	  }
	
	@RequestMapping(method = RequestMethod.POST, value="/author/update")
	  @ResponseBody
	  public Author updateAuthorById(@RequestBody Author author) {
		System.out.print("update for author = "+author.getAuthorname());
		authordao.updateAuthor(author);
		  return author;
	  }
	
	@RequestMapping(method = RequestMethod.DELETE, value="/author/delete")
	  @ResponseBody
	  public String deleteAuthorById(@RequestParam(value="authorid", required=true) String authorid) {
		authordao.deleteAuthor(authorid);
		  return authorid;
	  }
	
	@Autowired
	BookAuthorDao bookauthordao;
	@RequestMapping(method = RequestMethod.POST, value="/bookauthor/create")
	  @ResponseBody
	  public BookAuthor addBookAuthor(@RequestBody BookAuthor bookauthor) {
		System.out.println("getting data from book"+bookauthor);
		Book book = bookauthor.getBook();
		System.out.println("getting book id"+book.getBookid());
		System.out.println("getting book name"+book.getBookname());
		
	 	Book bookfromdb = bookdao.findOne(book.getBookid());
	 	if(bookfromdb == null) {
	 		bookdao.insertBook(book);
	 	}
//	 	else
//	 	{
//	 		book = bookfromdb;
//	 	}
	 	
		Author author = bookauthor.getAuthor();
	 	Author authorfromdb = authordao.findOne(author.getAuthorid());
	 	if(authorfromdb == null) {
	 		authordao.insertAuthor(author);
	 	}
	 	
	 	BookAuthor bookauthorfromdb  = bookauthordao.findOne(book.getBookid(), author.getAuthorid());
	 	if(bookauthorfromdb == null) {
	 		bookauthordao.insertBookAuthor(bookauthor);
	 	}
	 	
		  return bookauthor;
	  }
	
	@RequestMapping(method = RequestMethod.GET, value="/bookauthor/getid")
	  @ResponseBody
	  public List<BookAuthor> getByBookid(@RequestParam(value="bookid", required=true) String bookid) {
		return bookauthordao.findAllbyBookid(bookid);
		  //return book;
	  }
	
	@RequestMapping(method = RequestMethod.POST, value="/bookauthor/update")
	  @ResponseBody
	  public BookAuthor updateBookid(@RequestBody BookAuthor bookauthor){
		Book book = bookauthor.getBook();
		Author author = bookauthor.getAuthor();
		System.out.println("the value of book id "+book.getBookid());
		System.out.println("the value of book name "+book.getBookname());
		System.out.println("the value of author id "+author.getAuthorid());
		System.out.println("the value of author name "+author.getAuthorname());

		bookauthordao.updateByBookid(bookauthor);
		return bookauthor;
	  }
	
	@RequestMapping(method = RequestMethod.DELETE, value="/bookauthor/delete")
	  @ResponseBody
	  public String deleteBookid(@RequestParam(value="bookid", required=true) String bookid) {
		return bookauthordao.deleteByBookid(bookid);
		  //return book;
	  }
	
	@RequestMapping(method = RequestMethod.GET, value="/bookauthor/getall")
	  @ResponseBody
	  public List<BookAuthor> getAllBookauthors() {
		//System.out.println("request send !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		  return bookauthordao.findAllBookauthor(); 
	  }
	
}
