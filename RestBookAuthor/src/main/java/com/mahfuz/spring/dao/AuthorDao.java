package com.mahfuz.spring.dao;

import java.util.List;

import com.mahfuz.spring.model.Author;

public interface AuthorDao {
	
	List<Author> findAllAuthor();
	void insertAuthor(Author author);
	void updateAuthor(Author author);
	String deleteAuthor(String authorid);
	Author findOne(String authorid);
	List<Author> findbyAuthorid(String authorid);
}
