package com.mahfuz.spring.dao;

import java.util.List;

import com.mahfuz.spring.model.Book;

public interface BookDao {

	List<Book> findAllBook();
	List<Book> findbyBookid(String bookid);
	void insertBook(Book book);
	void updateBook(Book book);
	String deleteBook(String bookid);
	Book findOne(String bookid);
	
}
