package com.mahfuz.spring.dao;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.mahfuz.spring.mapper.AuthorRowMapper;
import com.mahfuz.spring.model.Author;

@Repository
public class AuthorDaoImpl implements AuthorDao{
	
	public AuthorDaoImpl(NamedParameterJdbcTemplate template) {  
        this.template = template;  
	}  

	NamedParameterJdbcTemplate template;
	
	@Override
	public Author findOne(String authorid) {
		List<Author> authorList = template.query("select * from authors where authorid ='"+authorid+"'", new AuthorRowMapper());
		return authorList.size()>0?authorList.get(0):null;
	}
	
	@Override
	public List<Author> findAllAuthor() {
		return template.query("select * from authors", new AuthorRowMapper());
	}

	@Override
	public void insertAuthor(Author author) {
		final String sql = "insert into authors(authorid, authorname) values(:authorId, :authorName)";
		
		 KeyHolder holder = new GeneratedKeyHolder();
		 SqlParameterSource param = new MapSqlParameterSource()
				 .addValue("authorId", author.getAuthorid())
				 .addValue("authorName", author.getAuthorname());
	 	 template.update(sql,param, holder);
	 	 //author.setAuthorid((int)holder.getKey());
	}

	@Override
	public void updateAuthor(Author author) {
		final String sql = "update authors set authorname=:authorName where authorid=:authorId";
		
		 //KeyHolder holder = new GeneratedKeyHolder();
		 SqlParameterSource param = new MapSqlParameterSource()
				 .addValue("authorId", author.getAuthorid())
				 .addValue("authorName", author.getAuthorname());
	 	 template.update(sql,param);
		
	}

	@Override
	public String deleteAuthor(String authorid) {
		final String sql = "delete from authors where authorid='"+authorid+"'";
		
		 //KeyHolder holder = new GeneratedKeyHolder();
		 SqlParameterSource param = new MapSqlParameterSource();
	 	 template.update(sql,param);
		 return "Author with ID " + authorid + " deleted successfully";
		
	}

	@Override
	public List<Author> findbyAuthorid(String authorid) {
		return template.query("select * from authors where authorid = '"+authorid+"'", new AuthorRowMapper());
	}

}
