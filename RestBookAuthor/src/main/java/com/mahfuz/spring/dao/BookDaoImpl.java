package com.mahfuz.spring.dao;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.mahfuz.spring.mapper.BookRowMapper;
import com.mahfuz.spring.model.Book;


@Repository
public class BookDaoImpl implements BookDao {
	
		public BookDaoImpl(NamedParameterJdbcTemplate template) {  
	        this.template = template;  
		}  

		NamedParameterJdbcTemplate template;  
		
		@Override
		public Book findOne(String bookid) {
			List<Book> bookList = template.query("select * from books where bookid ='"+bookid+"'", new BookRowMapper());
			return bookList.size()>0?bookList.get(0):null;
		}

		@Override
		public List<Book> findAllBook() {
			return template.query("select * from books", new BookRowMapper());
		}

		@Override
		public void insertBook(Book book) {
			 final String sql = "insert into books(bookid,bookname) values(:bookId,:bookName)";
	
			 KeyHolder holder = new GeneratedKeyHolder();
			 SqlParameterSource param = new MapSqlParameterSource() 
					 .addValue("bookId", book.getBookid())
					 .addValue("bookName", book.getBookname());
		 	 template.update(sql,param, holder);
		 	 //book.setBookid((int)holder.getKey());
		}

		@Override
		public void updateBook(Book book) {
			final String sql = "update books set bookname=:bookName where bookid=:bookId";
			
			 //KeyHolder holder = new GeneratedKeyHolder();
			 SqlParameterSource param = new MapSqlParameterSource()
					 .addValue("bookId", book.getBookid())
					 .addValue("bookName", book.getBookname());
		 	 template.update(sql,param);	
		}
		
		@Override
		public String deleteBook(String bookid) {
			final String sql = "delete from books where bookid='"+bookid+"'";
			
			 //KeyHolder holder = new GeneratedKeyHolder();
			 SqlParameterSource param = new MapSqlParameterSource();
					 
		 	 template.update(sql,param);
		 	return "Book with ID " + bookid + " deleted successfully";
		}

		@Override
		public List<Book> findbyBookid(String bookid) {
			return template.query("select * from books where bookid = '"+bookid+"'", new BookRowMapper());
		}

}
