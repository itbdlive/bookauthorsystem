package com.mahfuz.spring.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.mahfuz.spring.mapper.BookAuthorRowMapper;
import com.mahfuz.spring.model.Author;
import com.mahfuz.spring.model.Book;
import com.mahfuz.spring.model.BookAuthor;

@Repository
public class BookAuthorDaoImpl implements BookAuthorDao{
	
	public BookAuthorDaoImpl(NamedParameterJdbcTemplate template) {  
        this.template = template;  
	}  

	NamedParameterJdbcTemplate template;
	
	@Autowired
	BookDao bookdao;
	
	@Autowired
	AuthorDao authordao;
	
	@Override
	public BookAuthor findOne(String bookid, String authorid) {
		List<BookAuthor> bookauthorList = template.query("select * from bookauthors where bookid ='"+bookid+"' and authorid='"+authorid+"'", new BookAuthorRowMapper());
		return bookauthorList.size()>0?bookauthorList.get(0):null;
	}
	
	@Override
	public List<BookAuthor> findAllbyBookid(String bookid) {
		String query = "select books.bookid as bookid, books.bookname as bookname,"
				+ "authors.authorid as authorid, authors.authorname as authorname "
				+ "from books join bookauthors "
				+ "on books.bookid = bookauthors.bookid "
				+ "join authors "
				+ "on authors.authorid = bookauthors.authorid "
				+ "where books.bookid ='"+ bookid + "'";
		
		List<BookAuthor> bookauthorList = template.query(query, new BookAuthorRowMapper());
		return bookauthorList;
	}
	
	@Override
	public List<BookAuthor> findAllBookauthor(){
		//System.out.println("query stat for all list");
		String query = "select books.bookid as bookid, books.bookname as bookname, "
				+ "authors.authorid as authorid, authors.authorname as authorname "
				+ "from books join bookauthors "
				+ "on books.bookid = bookauthors.bookid "
				+ "join authors "
				+ "on authors.authorid = bookauthors.authorid ";
//		List<BookAuthor> bookauthorList = template.query(query, new BookAuthorRowMapper());
//		for(BookAuthor list : bookauthorList) {
//			System.out.println(list);
//		}
//		return bookauthorList;
		return template.query(query, new BookAuthorRowMapper());
	}

	@Override
	public void insertBookAuthor(BookAuthor bookauthor) {
			 	
	 	Book book = bookauthor.getBook();
		Author author = bookauthor.getAuthor();

		final String sql = "insert into bookauthors(bookid, authorid)values(:bookId, :authorId)";
		SqlParameterSource param1 = new MapSqlParameterSource()
			.addValue("bookId", book.getBookid())
			.addValue("authorId", author.getAuthorid());
	 	template.update(sql,param1);
		
	}
	
	@Override
	public void updateByBookid(BookAuthor bookauthor) {
		Book book = bookauthor.getBook();
		Author author = bookauthor.getAuthor();
		
		System.out.println("the value of book id "+book.getBookid());
		System.out.println("the value of book name "+book.getBookname());
		System.out.println("the value of author id "+author.getAuthorid());
		System.out.println("the value of author name "+author.getAuthorname());
		
		String bookquery = "UPDATE books set bookname=:bookname "
				+ "WHERE bookid = :bookid";
		String authorquery = "UPDATE authors set authorname=:authorname "
				+ "WHERE authorid = :authorid";
		
		SqlParameterSource param = new MapSqlParameterSource()
				.addValue("bookid", book.getBookid())
				.addValue("bookname", book.getBookname())
				.addValue("authorid", author.getAuthorid())
				 .addValue("authorname", author.getAuthorname());
		template.update(bookquery,param);
		template.update(authorquery,param);
		

	}
	
	@Override
	public String deleteByBookid(String bookid) {
		//System.out.println("bookid delete from db"+bookid);
		String query_delba = "delete from bookauthors where bookid= :bookid";
		String query_delb = "delete from books "
				+ "where bookid = :bookid";
		 SqlParameterSource param = new MapSqlParameterSource()
				 .addValue("bookid", bookid);
	 	 template.update(query_delba,param);
		//template.update(query_delba, params, types);
		template.update(query_delb, param);
		return "Book with ID " + bookid + " deleted successfully";
		//return null;
	}

	

}
