package com.mahfuz.spring.dao;

import java.util.List;

import com.mahfuz.spring.model.Book;
import com.mahfuz.spring.model.BookAuthor;

public interface BookAuthorDao {
	//List<BookAuthor> findAllAuthor();
	void insertBookAuthor(BookAuthor bookauthor);
	BookAuthor findOne(String bookid, String authorid);
	List<BookAuthor> findAllbyBookid(String bookid);
	//void updateBookAuthor(BookAuthor bookauthor);
	//void deleteBookAuthor(BookAuthor bookauthor);
	void updateByBookid(BookAuthor bookauthor);
	String deleteByBookid(String bookid);
	List<BookAuthor> findAllBookauthor();
}
