package com.mahfuz.thymeleaf.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.mahfuz.thymeleaf.form.BookAuthorForm;
import com.mahfuz.thymeleaf.model.Author;
import com.mahfuz.thymeleaf.model.Book;
import com.mahfuz.thymeleaf.model.BookAuthor;

@Controller
public class ClientServiceController {
	
	RestTemplate restTemplate = new RestTemplate();
 
    // Inject via application.properties
    @Value("${welcome.message}")
    private String message;
 
    @Value("${error.message}")
    private String errorMessage;
 
    @RequestMapping(value = { "/","/index" }, method = RequestMethod.GET)
    public String index(Model model) {
 
        model.addAttribute("message", message);
        return "index";
    }
    
    static final String URL_Books = "http://localhost:8081/book/getall";
    static final String URL_EditBooks = "http://localhost:8081/book/update";
    
    static final String URL_Authors = "http://localhost:8081/author/getall";
    static final String URL_EditAuthors = "http://localhost:8081/author/update";

    static final String URL_BookAuthors = "http://localhost:8081/bookauthor/getall";
    static final String URL_AddBookAuthors = "http://localhost:8081/bookauthor/create";
    static final String URL_EditBookAuthors = "http://localhost:8081/bookauthor/update/";
    
    @RequestMapping(value = { "/listBook" }, method = RequestMethod.GET)
    public String allBook(Model model) {

        Book books[] = restTemplate.getForObject(URL_Books, Book[].class);
        
        model.addAttribute("books", books);
        return "listBook";
    }
    
    @RequestMapping(value = { "/book/edit/{bookid}/{bookname}" }, method = RequestMethod.GET)
    public String editBookPage(@PathVariable("bookid") String bookid,@PathVariable("bookname") String bookname, Model model) {
 
        Book book = new Book();
        book.setBookid(bookid);
        book.setBookname(bookname);

        model.addAttribute("book",book);
 
        return "editBook";
    }
    
    @RequestMapping(value = { "/book/save" }, method = RequestMethod.POST)
    public String saveBook(@ModelAttribute  Book book) {
 
    	String bookid = book.getBookid();
        String bookname = book.getBookname();
        
        book.setBookid(bookid);
        book.setBookname(bookname);

        System.out.println("this is for update!!!!!!");
        System.out.println(book.getBookid());
        System.out.println(book.getBookname());
         
        HttpEntity<Book> request = new HttpEntity<>(book);
        Book book_ret = restTemplate.postForObject(URL_EditBooks, request, Book.class);

        return "redirect:/listBook";
            
    }
    
    @RequestMapping(value = { "/book/delete/{bookid}" },method = RequestMethod.GET)
    public String deleteBook(@PathVariable("bookid") String bookid) {
    	System.out.println("trying to delete "+bookid);
    	String URL_DeleteBooks = "http://localhost:8081/book/delete?bookid="+bookid;

        restTemplate.delete(URL_DeleteBooks);
    	return "redirect:/listBook";
    }
    
    
    
    @RequestMapping(value = { "/listAuthor" }, method = RequestMethod.GET)
    public String allAuthor(Model model) {
 
        // Send request with GET method and default Headers.
        Author authors[] = restTemplate.getForObject(URL_Authors, Author[].class); 
        model.addAttribute("authors", authors);
        return "authorList";
    }
    
    @RequestMapping(value = { "/author/edit/{authorid}/{authorname}" }, method = RequestMethod.GET)
    public String editAuthorPage(@PathVariable("authorid") String authorid,@PathVariable("authorname") String authorname, Model model) {
 
        Author author = new Author();
        author.setAuthorid(authorid);
        author.setAuthorname(authorname);

        model.addAttribute("author",author);
 
        return "editAuthor";
    }
    
    @RequestMapping(value = { "/author/save" }, method = RequestMethod.POST)
    public String saveAuthor(@ModelAttribute  Author author) {
 
    	String authorid = author.getAuthorid();
        String authorname = author.getAuthorname();
        
        author.setAuthorid(authorid);
        author.setAuthorname(authorname);

        System.out.println("this is for update!!!!!!");
        System.out.println(author.getAuthorid());
        System.out.println(author.getAuthorname());
         
        HttpEntity<Author> request = new HttpEntity<>(author);
        Author author_ret = restTemplate.postForObject(URL_EditAuthors, request, Author.class);

        return "redirect:/listAuthor";
            
    }
    
    @RequestMapping(value = { "/author/delete/{authorid}" },method = RequestMethod.GET)
    public String deleteAuthor(@PathVariable("authorid") String authorid) {
    	System.out.println("trying to delete "+authorid);
    	String URL_DeleteAuthors = "http://localhost:8081/author/delete?authorid="+authorid;
        restTemplate.delete(URL_DeleteAuthors);
       
        return "redirect:/listAuthor";
       
    }
 
    @RequestMapping(value = { "/listBookAuthor" }, method = RequestMethod.GET)
    public String listBookAuthor(Model model) {
 
    	// Send request with GET method and default Headers.
        BookAuthor bookauthors[] = restTemplate.getForObject(URL_BookAuthors, BookAuthor[].class);
        model.addAttribute("bookauthors", bookauthors);
        return "listBookAuthor";
    }
// 
    @RequestMapping(value = { "/createBookAuthor" }, method = RequestMethod.GET)
    public String AddBookAuthorPage(Model model) {
 
        BookAuthorForm bookauthorForm = new BookAuthorForm();
        model.addAttribute("bookauthorForm", bookauthorForm);
 
        return "addBookAuthor";
    }
 
    @RequestMapping(value = { "/createBookAuthor" }, method = RequestMethod.POST)
    public String saveBookAuthor(@ModelAttribute  BookAuthorForm bookauthorForm) {
 
        String bookid = bookauthorForm.getBookid();
        String bookname = bookauthorForm.getBookname();
        String authorid = bookauthorForm.getAuthorid();
        String authorname = bookauthorForm.getAuthorname();
        
        Book book = new Book();
        Author author = new Author();
        book.setBookid(bookid);
        book.setBookname(bookname);
        author.setAuthorid(authorid);
        author.setAuthorname(authorname);
      
        BookAuthor bookauthor = new BookAuthor();
        bookauthor.setBook(book);
        bookauthor.setAuthor(author);
        
        System.out.println(book.getBookid());
        System.out.println(book.getBookname());
        System.out.println(author.getAuthorid());
        System.out.println(author.getAuthorname());
         
        HttpEntity<BookAuthor> request = new HttpEntity<>(bookauthor);
        BookAuthor bookauthor_ret = restTemplate.postForObject(URL_AddBookAuthors, request, BookAuthor.class);

        return "redirect:/listBookAuthor";
            
    }
    
    @RequestMapping(value = { "/editBookAuthor/{bookid}/{bookname}/{authorid}/{authorname}" }, method = RequestMethod.GET)
    public String editBookAuthorPage(@PathVariable("bookid") String bookid,@PathVariable("bookname") String bookname,
    		@PathVariable("authorid") String authorid,@PathVariable("authorname") String authorname, Model model) {
 
        BookAuthorForm bookauthorForm = new BookAuthorForm();

        bookauthorForm.setBookid(bookid);
        bookauthorForm.setBookname(bookname);
        bookauthorForm.setAuthorid(authorid);
        bookauthorForm.setAuthorname(authorname);
        
        model.addAttribute("bookauthorForm",bookauthorForm);
 
        return "editBookAuthor";
    }

    @RequestMapping(value = { "/editBookAuthor" }, method = RequestMethod.POST)
    public String editBookAuthor(@ModelAttribute  BookAuthorForm bookauthorForm) {
 
    	String bookid = bookauthorForm.getBookid();
        String bookname = bookauthorForm.getBookname();
        String authorid = bookauthorForm.getAuthorid();
        String authorname = bookauthorForm.getAuthorname();
        
        Book book = new Book();
        Author author = new Author();
        book.setBookid(bookid);
        book.setBookname(bookname);
        author.setAuthorid(authorid);
        author.setAuthorname(authorname);
      
        BookAuthor bookauthor = new BookAuthor();
        bookauthor.setBook(book);
        bookauthor.setAuthor(author);
        System.out.println("this is for update!!!!!!");
        System.out.println(book.getBookid());
        System.out.println(book.getBookname());
        System.out.println(author.getAuthorid());
        System.out.println(author.getAuthorname());
         
        HttpEntity<BookAuthor> request = new HttpEntity<>(bookauthor);
        BookAuthor bookauthor_ret = restTemplate.postForObject(URL_EditBookAuthors, request, BookAuthor.class);

        return "redirect:/listBookAuthor";      
    }

    @RequestMapping(value = { "/deleteBookAuthor/delete/{bookid}" },method = RequestMethod.GET)
    public String deleteBookAuthor(@PathVariable("bookid") String bookid) {
    	System.out.println("trying to delete "+bookid);
    	
    	String URL_DeleteBookAuthors = "http://localhost:8081/bookauthor/delete?bookid="+bookid;
        restTemplate.delete(URL_DeleteBookAuthors);
    	return "redirect:/listBookAuthor";
    }
 
}
