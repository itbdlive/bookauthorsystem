package com.mahfuz.thymeleaf.form;

public class AuthorForm {
	
	String authorid;
	String authorname;
	
	public AuthorForm(String authorid, String authorname) {
		this.authorid = authorid;
		this.authorname = authorname;
	}
	public String getAuthorid() {
		return authorid;
	}
	public void setAuthorid(String authorid) {
		this.authorid = authorid;
	}
	public String getAuthorname() {
		return authorname;
	}
	public void setAuthorname(String authorname) {
		this.authorname = authorname;
	}

}
