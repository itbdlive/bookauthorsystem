package com.mahfuz.thymeleaf.form;

public class Form {
	
	BookForm bookform;
	AuthorForm authorform;
	
	public Form(BookForm bookform, AuthorForm authorform) {
		this.bookform = bookform;
		this.authorform = authorform;
	}
	
	
	public BookForm getBookform() {
		return bookform;
	}
	public void setBookform(BookForm bookform) {
		this.bookform = bookform;
	}
	public AuthorForm getAuthorform() {
		return authorform;
	}
	public void setAuthorform(AuthorForm authorform) {
		this.authorform = authorform;
	}
}
