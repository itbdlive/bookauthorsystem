package com.mahfuz.thymeleaf.form;

public class BookForm {
	
	String bookid;
	String bookname;
	
	public BookForm(String bookid, String bookname) {
		this.bookid = bookid;
		this.bookname = bookname;
	}
	
	public String getBookid() {
		return bookid;
	}
	public void setBookid(String bookid) {
		this.bookid = bookid;
	}
	public String getBookname() {
		return bookname;
	}
	public void setBookname(String bookname) {
		this.bookname = bookname;
	}
	

}
