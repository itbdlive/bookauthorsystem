package com.mahfuz.thymeleaf.model;

import com.mahfuz.thymeleaf.model.Author;
import com.mahfuz.thymeleaf.model.Book;

public class BookAuthor {
	 
	Book book = new Book();
	Author author = new Author();
	
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public Author getAuthor() {
		return author;
	}
	public void setAuthor(Author author) {
		this.author = author;
	}
 
    
 
}