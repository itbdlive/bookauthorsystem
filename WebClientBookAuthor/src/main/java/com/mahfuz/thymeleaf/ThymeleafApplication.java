package com.mahfuz.thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import com.mahfuz.thymeleaf.model.Book;

@SpringBootApplication
public class ThymeleafApplication {
	
	static final String URL_Books = "http://localhost:8083/book/getall";
	static final String URL_Authors = "http://localhost:8083/author/getall";

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafApplication.class, args);
		
//		RestTemplate restTemplate = new RestTemplate();
		 
        // Send request with GET method and default Headers.
//        Book books[] = restTemplate.getForObject(URL_Books, Book[].class);
//        String author = restTemplate.getForObject(URL_Authors, String.class);
        
//        if (books != null) {
//            for (Book book : books) {
//                System.out.println("Book : " + book.getBookid() + " - " + book.getBookname());
//            }
// 
//        }
//        System.out.println(author);
	}
}
